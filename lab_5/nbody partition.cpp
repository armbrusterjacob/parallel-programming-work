#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <omp.h>
#include <cmath>

const double G = 1.0;
const double STEP = 0.01;
const int NODE_COUNT = 1000;
// nodes shouldn't step out 200x200 bounds if program runs correctly.

// Store x and y coordinates to be used by Node
struct Coord
{
    double x;
    double y;

    Coord(double _x, double _y) : x(_x), y(_y){;}
    Coord() : x(0), y(0) {;}
};

// A repsentation of a body in the n-body problem
struct Node
{
    double mass;

    Coord velocity;
    Coord force;
    Coord pos;

    Node(double _x, double _y) : pos(Coord(_x,_y)), mass(1){;}
};

void calculateForce(std::vector<Node>& nodes, const int& rank, const int& tCount)
{
    // start k equal to the thread num as ` not repeat calculations
    for(size_t q = rank * NODE_COUNT / tCount; q < ((rank + 1) * NODE_COUNT / tCount); q++)
    {
        for(size_t k = q + 1; k < NODE_COUNT; k++)
        {
            double x_diff = nodes[q].pos.x - nodes[k].pos.x;
            double y_diff = nodes[q].pos.y - nodes[k].pos.y;
            double dist = std::sqrt((x_diff * x_diff) + (y_diff * y_diff));
            if(dist < 0.1)
                continue;
            double dist_cubed = dist * dist * dist;
            Coord force_qk;
            force_qk.x = - G * nodes[q].mass * nodes[k].mass / dist_cubed * x_diff;
            force_qk.y = - G * nodes[q].mass * nodes[k].mass / dist_cubed * y_diff;
            #pragma omp critical
            {
            nodes[q].force.x += force_qk.x;
            nodes[q].force.y += force_qk.y;
            nodes[k].force.y -= force_qk.y;
            nodes[k].force.x -= force_qk.x;
            }
        }
    }
    return;
};

void updateBody(std::vector<Node>& nodes, const int& rank, const int& tCount)
{
    for(size_t q = rank * NODE_COUNT / tCount; q < ((rank + 1) * NODE_COUNT / tCount); q++)
    {
        nodes[q].velocity.x += nodes[q].force.x * STEP / nodes[q].mass;
        nodes[q].pos.x +=  nodes[q].velocity.x * STEP;
        nodes[q].velocity.y += nodes[q].force.y * STEP / nodes[q].mass;
        nodes[q].pos.y +=  nodes[q].velocity.y * STEP;
    }

    return;
}

void resetForces(std::vector<Node>& nodes, const int& rank, const int& tCount)
{
    for(size_t q = rank * NODE_COUNT / tCount; q < ((rank + 1) * NODE_COUNT / tCount); q++)
    {
        nodes[q].force.x = 0;
        nodes[q].force.y = 0;
    }
}

int main(int argc, char* argv[])
{
    int thread_count = 8;
    int iterations = 100;
    std::string filename = "nbodies.dat";

    // If provided get data from command line
    if(argc > 1)
        thread_count = strtol(argv[1], NULL,  10);
    if(argc > 2)
        filename = argv[2];
    if(argc > 3)
        iterations = strtol(argv[3], NULL,  10);

    // Get data from file insert it into a node vector
    std::vector<Node> nodes;
    std::fstream file(filename.c_str());
    if(file.fail())
    {
        std::cout << "Couldn't open file, \'" << filename << "\'." << std::endl;
        return -1;
    }
    while(!file.eof())
    {
        double x, y;
        file >> x;
        file >> y;
        nodes.push_back(Node(x,y));
    }

    // Print a predermied amount of nodes from the start of the vector at their initial state
    const int PRINT_COUNT = 20;
    std::cout << "Starting Positions:" << std::endl;
    for(int i = 0; i < PRINT_COUNT; i++)
    {
        std::cout << "Node("  << std::setw(4) <<  i << "): ";
        std::cout << "x=" << std::fixed << std::setw(7) << std::setprecision(3) << std::setfill( ' ' ) << nodes[i].pos.x << "   ";
        std::cout << "y=" << std::fixed << std::setw(7) << std::setprecision(3) << std::setfill( ' ' ) <<  nodes[i].pos.y;
        std::cout << std::endl;
    }
    std::cout << std::endl;

    // For each node go through every iteration calcuating new positons based on calulated forces
    #pragma omp parallel num_threads(thread_count)
    {
        int thread = omp_get_thread_num();

        for(size_t i = 0; i < iterations; i++)
        {   
            resetForces(nodes, thread, thread_count);
            #pragma omp barrier
            calculateForce(nodes, thread, thread_count);
            #pragma omp barrier
            updateBody(nodes, thread, thread_count);
        }
    }

    // Print a predermied amount of nodes from the start of the vector at their end state
    std::cout << "Ending Positions:" << std::endl;
    for(int i = 0; i < PRINT_COUNT; i++)
    {
        std::cout << "Node("  << std::setw(4) <<  i << "): ";
        std::cout << "x=" << std::fixed << std::setw(7) << std::setprecision(3) << std::setfill( ' ' ) << nodes[i].pos.x << "   ";
        std::cout << "y=" << std::fixed << std::setw(7) << std::setprecision(3) << std::setfill( ' ' ) <<  nodes[i].pos.y;
        std::cout << std::endl;
    }
    std::cout << std::endl;
    

    file.close();
    
  
  return 0;
}