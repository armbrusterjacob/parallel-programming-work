#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <mpi.h>

const int END_STATE = -1;

void receivepotato(int& potato);
void Initializepotato(const int& rank, const int& processorCount);
int chooseOtherProccessor(const int& rank, const int& processorCount);
void passpotato(int& potato, const int& rank, const int& processorCount);
void endGame(const int& rank, const int& size);

int main(int argc, char *argv[])
{
    int rank, size, potato;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if(size < 2)
    {
        std::cout << "It takes at least two players to play. (processorCount = " << size << ")." << std::endl;
        return 0;
    }

    srand(time(NULL));

    // Node 0 is the ringmaster
    if(rank == 0)
    {
        Initializepotato(rank, size);
    }

    receivepotato(potato);
    while(potato != END_STATE)
    {
        if(potato == 0)
        {
            endGame(rank, size);
            break;
        } else
        {
            passpotato(potato, rank, size);
            receivepotato(potato);
        }
    }

    MPI_Finalize();

    return 0;
}

void receivepotato(int& potato)
{
    //MPI_Recv(recv_buffer, buffer_size, mpi_type, source_rank, recv_tag, recv_comm, &status)
    MPI_Status status;
    MPI_Recv(&potato, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
}

void Initializepotato(const int& rank, const int& processorCount)
{
    const int RANGE_MULTIPLIER = 2;
    int range = RANGE_MULTIPLIER * processorCount;

    int potato = processorCount + (rand() % range + 1);
    std::cout << "Initial potato count: " << potato << std::endl;
    passpotato(potato, rank, processorCount);
    return;
}

int chooseOtherProccessor(const int& rank, const int& processorCount)
{
    int choice = rand() % (processorCount - 1);
    if(choice >= rank)
    {
        choice++;
    }
    return choice;
}

void passpotato(int& potato, const int& rank, const int& processorCount)
{
    potato--;
    int recipiant = chooseOtherProccessor(rank, processorCount);
    std::cout << "Node " << rank << " is sending the potato to " << recipiant << "." << std::endl;

    //MPI_Send(send_buffer, buffer_size, mpi_type, destination_rank, send_tag, send_comm);
    MPI_Send(&potato, 1, MPI_INT, recipiant, 0, MPI_COMM_WORLD);
}

void endGame(const int& rank, const int& size)
{   
    for(size_t i = 0; i < size; i++)
    {
        if(i != rank)
        {
            int a = END_STATE;
            MPI_Send(&a, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
    }
}