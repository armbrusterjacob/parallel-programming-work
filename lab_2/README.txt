Parallel Processing Lab 1
Name: Jacob Armbruster

(Same text is included in docx file)

In this project we were assigned to make a program that simulates the game "Hot potato" using the the an MPI library. In the program one node is assigned to be a ringmaster who then initializes the potato which is simulated as an integer. It's randomly assigned an integer that's higher than the amount of processors provided to the program. This then starts the chain of passing the potato by choosing a random node other than itself continuously. Each time the potato is passed the counter on the potato is decremented until it finally reaches 0. The node which receives a potato with a 0 counter is "it" and then becomes the node which notifies the rest of the nodes that the game is over and for their processing to end.

The result is a working program that can work with any processor count above 2 (player count). That working program will in fact simulate Hot potato as required. In addition I also learned how to use the six main functions in MPI. That is, MPI_Init, MPI_Finalize, MPI_Send, MPI_Recv, MPI_Rank, and MPI_Size.
