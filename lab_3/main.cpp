#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>
#include <mpi.h>

// Currently the program is assumed to use 4 processors
const size_t NODE_COUNT = 4;
const size_t M = 16;
const size_t N = 16;
const size_t CENTER_M = (M / 2);
const size_t CENTER_N = (N / 2);
const size_t SUB_N = CENTER_N + 2;
const size_t SUB_M = CENTER_M + 2;
const int FILE_END = -1;

enum state
{
    dead,
    alive,
};

template <size_t size_x, size_t size_y>
void initializeGameFromFile(bool (&arr)[size_x][size_y], char* filename);
template <size_t size_x, size_t size_y>
void printGamestate(bool (&arr)[size_x][size_y]);
template <size_t size_x, size_t size_y>
void updateGamestate(bool (&arr)[size_x][size_y], int rank);
template <size_t size_x, size_t size_y>
void applyRules(bool (&arr)[size_x][size_y]);
template <size_t size_x, size_t size_y>
void initilizeArray(bool (&arr)[size_x][size_y]);
template <size_t size_x, size_t size_y>
void updateBorder(bool (&arr)[size_x][size_y], int rank);

int main (int argc, char* argv[])
{
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Status status;

    bool printEach = false;
    int iterations = 10; // iterations
    char* filename;

    if(argc > 2)
    {
       iterations = atoi(argv[1]);
       filename = argv[2]; 
    }
    if(argc > 3)
    {
       printEach = atoi(argv[3]);
    }else
    {
        std::cout << "incorrect parameters given" << std::endl;
        exit(0);
    }

    bool gamestate[M][N];
    bool subgame[SUB_M][SUB_N];
    
    initilizeArray(gamestate);
    initilizeArray(subgame);

    int nodeSqr = (int) sqrt(NODE_COUNT);

    // Make the first node initialize the board from a filename if there is a filename
    if(rank == 0 && filename)
    {
        initializeGameFromFile(gamestate, filename);

        // Transfer to leader's subgame array
        for(size_t i = 1; i < SUB_M - 1; i++)
        {
            for(size_t j = 1; j < SUB_N - 1; j++)
            {
                subgame[i][j] = gamestate[i-1][j-1];
            }
        }

        // Transfer to every other node's subgame array
        // Sends row by row a block of the 2D 
        for(size_t i = 1; i < size; i++)
        {
            // defines what sector on the x-axis a block is in
            int x_base = (CENTER_M) * (i % nodeSqr);
            // defines what sector on the y-axis a block is in.
            int y_base = (CENTER_N) * (i / nodeSqr % nodeSqr);
            
            for(size_t j = 0; j < CENTER_N; j++)
            {
                MPI_Send(&gamestate[y_base + j][x_base], CENTER_M, MPI::BOOL, i, j, MPI_COMM_WORLD);
            }  
        }
    }else{

        for(size_t i = 1; i < SUB_N - 1; i++)
        {
            MPI_Recv(&subgame[i][1], CENTER_M, MPI::BOOL, 0, i - 1, MPI_COMM_WORLD, &status);
        }
    }

    if(rank == 0)
    {
        std::cout << "initial state:" << std::endl;
        printGamestate(gamestate);
    }

    // for every iteration
    for(int i = 0; i < iterations; i++)
    {
        updateBorder(subgame, rank);
        applyRules(subgame);

        // Slower if printing because of numerous more MPI calls
        if(printEach)
        {
            if(rank == 0)
            {
                for(size_t i = 1; i < SUB_M + 1; i++)
                {
                    for(size_t j = 1; j < SUB_N + 1; j++)
                    {
                        gamestate[i-1][j-1] = subgame[i][j];
                    }
                }

                for(size_t i = 1; i < size; i++)
                {
                    // defines what sector on the x-axis a block is in
                    int x_base = (CENTER_M) * (i % nodeSqr);
                    // defines what sector on the y-axis a block is in.
                    int y_base = (CENTER_N) * (i / nodeSqr % nodeSqr);
                    for(size_t j = 0; j < CENTER_M; j++)
                    {
                        MPI_Recv(&gamestate[y_base + j][x_base], CENTER_M, MPI::BOOL, i, j, MPI_COMM_WORLD, &status);
                    }
                }

                //print final state
                std::cout << "iteration: " << i << std::endl;
                printGamestate(gamestate);
            }else
            {
                for(size_t i = 1; i < SUB_N - 1; i++)
                {
                    MPI_Send(&subgame[i][1], CENTER_M, MPI::BOOL, 0, i - 1, MPI_COMM_WORLD);
                }
            }
        }
    }

    if(!printEach)
    {
        if(rank == 0)
        {
            for(size_t i = 1; i < SUB_M + 1; i++)
            {
                for(size_t j = 1; j < SUB_N + 1; j++)
                {
                    gamestate[i-1][j-1] = subgame[i][j];
                }
            }

            for(size_t i = 1; i < size; i++)
            {
                // defines what sector on the x-axis a block is in
                int x_base = (CENTER_M) * (i % nodeSqr);
                // defines what sector on the y-axis a block is in.
                int y_base = (CENTER_N) * (i / nodeSqr % nodeSqr);
                for(size_t j = 0; j < CENTER_M; j++)
                {
                    MPI_Recv(&gamestate[y_base + j][x_base], CENTER_M, MPI::BOOL, i, j, MPI_COMM_WORLD, &status);
                }
            }

            //print final state
            std::cout << "final iteration: " << std::endl;
            printGamestate(gamestate);
        }else
        {
            for(size_t i = 1; i < SUB_N - 1; i++)
            {
                MPI_Send(&subgame[i][1], CENTER_M, MPI::BOOL, 0, i - 1, MPI_COMM_WORLD);
            }
        }
    }

    MPI_Finalize();

    return 0;
}

template <size_t size_x, size_t size_y>
void updateBorder(bool (&subgame)[size_x][size_y], int rank)
{
    // note that because we know we're only running this through 4 processors
    // we only need to consider blocks that are in the corners if we were
    // to add more we need to consider wall blocks and center blocks
    // Additonally nodes are sent from in the orders right->left, left->right
    // and down->up, up->down to avoid deadlock

    MPI_Status status;
    bool vert_border[CENTER_N + 1];

    int nodeSqr = (int) sqrt(NODE_COUNT);
    int x_pos = rank % nodeSqr;
    int y_pos = rank / nodeSqr % nodeSqr;

    // If node on the top side
    if(y_pos == 0)
    {
        MPI_Send(&subgame[SUB_N - 2][1], CENTER_N, MPI::BOOL, rank + 2, 0, MPI_COMM_WORLD);
        MPI_Recv(&subgame[SUB_N - 1][1], CENTER_N, MPI::BOOL, rank + 2, 0, MPI_COMM_WORLD, &status);
    }else
    {
        MPI_Recv(&subgame[0][1], CENTER_N, MPI::BOOL, rank - 2, 0, MPI_COMM_WORLD, &status);
        MPI_Send(&subgame[1][1], CENTER_N, MPI::BOOL, rank - 2, 0, MPI_COMM_WORLD);
    }

    // We also need to account for the inner corners, we do so by using the already transfered
    // horizontal cells into a the end of vert_border as to save an additonal MPI call
    // If the node is on the left side
    if(x_pos == 0)
    {
        for(size_t i = 0; i < CENTER_N + 1; i++)
        {
            vert_border[i] = subgame[i + 1][SUB_M - 2];
        }
        if(y_pos == 0)
        {
            vert_border[CENTER_N] = subgame[SUB_N - 1][SUB_M - 2];
        }
        else
        {
            vert_border[CENTER_N] = subgame[0][SUB_M - 2];
        }
        MPI_Send(vert_border, CENTER_N + 1, MPI::BOOL, rank + 1, 0, MPI_COMM_WORLD);

        MPI_Recv(vert_border, CENTER_N + 1, MPI::BOOL, rank + 1, 0, MPI_COMM_WORLD, &status);
        for(size_t i = 0; i < CENTER_N; i++)
        {
            subgame[i + 1][SUB_N - 1] = vert_border[i];
        }
        if(y_pos == 0)
        {
            subgame[SUB_N - 1][SUB_M - 1] = vert_border[CENTER_N];
        }
        else
        {
            subgame[0][SUB_M - 1] = vert_border[CENTER_N];
        }
    }else
    {
        MPI_Recv(vert_border, CENTER_N + 1, MPI::BOOL, rank - 1, 0, MPI_COMM_WORLD, &status);
        for(size_t i = 0; i < CENTER_N; i++)
        {
            subgame[i + 1][0] = vert_border[i];
        }
        if(y_pos == 0)
        {
            subgame[SUB_N - 1][0] = vert_border[CENTER_N];
        }
        else
        {
            subgame[0][0] = vert_border[CENTER_N];
        }

        for(size_t i = 0; i < CENTER_N; i++)
        {
            vert_border[i] = subgame[i + 1][1];
        }
        if(y_pos == 0)
        {
            vert_border[CENTER_N] = subgame[SUB_N - 1][1];
        }
        else
        {
            vert_border[CENTER_N] = subgame[0][1];
        }
        MPI_Send(vert_border, CENTER_N + 1, MPI::BOOL, rank - 1, 0, MPI_COMM_WORLD);
    }
}

template <size_t size_x, size_t size_y>
void initilizeArray(bool (&arr)[size_x][size_y])
{
    for(size_t i = 0; i < size_x; i++)
    {
        for(size_t j = 0; j < size_y; j++)
        {
            arr[i][j] = state::dead;
        }
    }
}

template <size_t size_x, size_t size_y>
void initializeGameFromFile(bool (&arr)[size_x][size_y], char* filename)
{
    std::ifstream file(filename, std::ios::in);
    if(file.fail())
    {
        std::cout << "File not found." << std::endl;
        return;
    }
    
    std::string str;
    while(getline(file,str))
    {
        int x_pos = -1, y_pos = -1;
        int previousBreak = 0;
        int wordLength = 0;
        for(size_t i = 0; i < str.size(); i++)
        {
            if(i + 1 == str.size())
            {
                if(x_pos != -1)
                {
                    y_pos = std::atoi(str.substr(previousBreak, wordLength).c_str());
                    if(x_pos < 0 || y_pos < 0 || x_pos >= size_x || y_pos >= size_y)
                    {
                        std::cout << "Invalid coordinates" << std::endl;
                    }
                    else{
                        // std::cout << x_pos << ',' << y_pos << std::endl;
                        arr[x_pos][y_pos] = state::alive;
                    }
                }
            }
            else if(str[i] == ' ')
            {
                previousBreak = i + 1;
                x_pos = std::atoi(str.substr(0, wordLength).c_str());
                wordLength = 1;
                if(x_pos == FILE_END)
                {
                    break;
                }
            }else{
                wordLength++;
            }
        }
    }
}

template <size_t size_x, size_t size_y>
void printGamestate(bool (&arr)[size_x][size_y])
{
    std::cout  << "|" << std::string(size_x * 2, '-') << "|" << std::endl;
    for(size_t i = 0; i < size_x; i++)
    {
        std::cout << "|";
        for(size_t j = 0; j < size_y; j++)
        {
            if(arr[i][j] == state::alive)
            {
                std::cout << "██";
            }else
            {
                std::cout << "░░";
            }
        }
        std::cout << "|" << std::endl;
    }
    std::cout  << "|" << std::string(size_x * 2, '-') << "|" << std::endl;
}

template <size_t size_x, size_t size_y>
void updateGamestate(bool (&arr)[size_x][size_y], int rank)
{
    // exlude "borders" of the sub-arrays
    int x_start = (CENTER_M) * (rank % (int) sqrt(NODE_COUNT));
    int y_start = (CENTER_N) * (rank % (int) sqrt(NODE_COUNT));
    std::cout << rank << ": " << x_start << std::endl;
    for(size_t i = 1; i < SUB_N - 1; i++)
    {   
        // bool* row_ptr = 4
        // MPI_Gather(send_data, send_count, send_datatype, recv_data, recv_count, recv_datatype, root, communicator)
    }
    
}

template <size_t size_x, size_t size_y>
void applyRules(bool (&subgame)[size_x][size_y])
{
    bool prev_state[SUB_M][SUB_N];
    for(size_t i = 0; i < SUB_M; i++)
    {
        for(size_t j = 0; j < SUB_N; j++)
        {
            prev_state[i][j] = subgame[i][j];
        }   
    }

    for(size_t i = 1; i < CENTER_M + 1; i++)
    {
        for(size_t j = 1; j < CENTER_N + 1; j++)
        {
            const int NUM_BORDERING = 4;
            // add all eight neighbors
            int num_alive = prev_state[i][j + 1] + prev_state[i][j - 1] + prev_state[i + 1][j] + prev_state [i - 1][j]
                + prev_state[i - 1][j - 1] + prev_state[i + 1][j - 1] + prev_state[i - 1][j + 1] + prev_state[i + 1][j + 1];
            int num_dead = NUM_BORDERING - num_alive;

            if(prev_state[i][j] == state::dead && num_alive == 3)
            {
                subgame[i][j] = state::alive;
            }
            if(prev_state[i][j] == state::alive && (num_alive != 2 && num_alive != 3))
            {
                subgame[i][j] = state::dead;
            }
            else if(prev_state[i][j] == state::alive)
            {
            }
        }
    }
}