#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>
#include <mpi.h>

int main (int argc, char* argv[])
{
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Status status;

    if(rank == 0){
        bool arr[2][8];
        
        
        for(size_t i = 0; i < 2; i++)
        {
            for(size_t j = 0; j < 8; j++)
            {
                if((i + j) % 19 != 18)
                    arr[i][j] = 1;
                else
                    arr[i][j] = 0;
            }
        }

        std::cout << "rank: " << rank << "- " << std::endl;
        for(size_t i = 0; i < 2; i++)
        {
            std::cout << "    ";
            for(size_t j = 0; j < 8; j++)
            {
                if(arr[i][j])
                    std::cout << "1 ";
                if(!arr[i][j])
                    std::cout << "0 ";
            }
            std::cout << std::endl;
        }
        
        std::cout << std::endl;

        
        MPI_Send(&arr[0][0], 4, MPI::BOOL, 1, 0, MPI_COMM_WORLD);
        

    }else
    {
        bool arr[2][8];

        
        for(size_t j = 0; j < 8; j++)
        {
            for(size_t i = 0; i < 2; i++)
            {
                arr[i][j] = 0;
            }
        }
        
        std::cout << arr << std::endl;
        std::cout << arr + 4 << std::endl;
        std::cout << &arr[0][0] << std::endl;
        std::cout << &arr[0][0] + 4 << std::endl;
        MPI_Recv(arr + 4, 4, MPI::BOOL, 0, 0, MPI_COMM_WORLD, &status);
        
        std::cout << "rank: " << rank << "- " << std::endl;
        for(size_t i = 0; i < 2; i++)
        {
            std::cout << "    ";
            for(size_t j = 0; j < 8; j++)
            {
                if(arr[i][j])
                    std::cout << "1 ";
                if(!arr[i][j])
                    std::cout << "0 ";
            }
            std::cout << std::endl;
        }
        
        std::cout << std::endl;

    }


    MPI_Finalize();

    return 0;
}