Parallel Programming Lab 3
Name: Jacob Armbruster

In this lab we were assigned to simulate Conway's Game of Life. This is a problem
carried out on a 2D board, or array where cells are state to be dead or alive.
Fist an initial state is given via file input. Then for each iteration rules are applied
that dictate for every cell if it will remain alive or dead towards in the next iteration.
In the version we were assigned we were to divide the 2D array between 4 processors
using MPI. This was carried out by first having the lead node distrubte each "block" of
the array to the node it need to be at, including itself. Then for each iteration
each node will transfer both it's horizontal and vertical border, and also the corner
value for it's vertical neighbor to the appropiate reciver. Send and recieves are in the
order top->bottom, bottom->up and left->right, right->left to avoid deadlock. Now with 
each node having the correct subarray they apply the rules for each cell within it.
The rules require we have the borders, and that the outer perimiter of the array be
permenetly considered dead cells. Finally the subarrays build back up the main array
and the final state is printed.

In order to build and run this project here is an example of commands:

mpic++ -o lab_3 main.cpp
mpiexec -n 4 ./lab_3 10 inputs/pulsar.txt 0

The last parameter will let you print out each iteration when it's '1' but is
slower as it requires more mpi calls for each iteration.