#include <iostream>
#include <cstdlib>
#include <cmath>
#include <mpi.h>

/*  only works when the number of processors is low because
   otherwise you start overflowing integers with factorials
   and as a result get incorrect sums.
*/

double factorial(double num);

int main(int argc, char *argv[])
{
   int rank, size;
   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Comm_size(MPI_COMM_WORLD, &size);

   float x;
   if(argc > 1)
   {
       x = atof(argv[1]);
   }else
   {
       std::cout << "no x given" << std::endl;
       exit(0);
   }
   int n = 1 + rank;
   float buf = (pow(x, n + 1)) / factorial(n + 1);
   float sum = 0;
   //MPI_Reduce(sendbuf, recbuf, count, mpi_type, mpi_op, root, mpi_comm);
   MPI_Reduce(&buf, &sum, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
   if(rank == 0)
   {
       float answer = 1 + x + sum;
       std::cout << "e^" << x << " is estimated to be: " << answer << std::endl;
   }

   MPI_Finalize();

   return 0;

}

double factorial(double num)
{
   if(num == 0 || num == 1)
   {
       return num;
   }else
   {
       return num * factorial(num - 1);
   }
}

