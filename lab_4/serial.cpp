#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>
#include <cstdlib>
#include <fstream>

struct edge
{
    public:
    int start;
    int end;
    int weight;
};

int main(int argc, char* argv[])
{
    char* filename;
    if(argc > 1)
    {
        filename = argv[1];
    }
    else
    {
        std::cout << "Provide a filename" << std::endl;
    }

    // ... set up file ...
    std::ifstream file(filename, std::ios::in);
    std::string str;
    file >> str;

    std::cout << str << std::endl;
    int vector_count = std::atoi(str.c_str());

    std::vector<std::string> cities;
    std::vector<edge*> edges;

    
    for(size_t i = 0; i < vector_count; i++)
    {
        file >> str;
        cities.push_back(str);
    }
    
    while(true)
    {
        file >> str;
        int start = std::atoi(str.c_str());
        if(start == -1)
        {
            break;
        }
        file >> str;
        int end = std::atoi(str.c_str());
        file >> str;
        int weight = std::atoi(str.c_str());

        edge* my_edge = new edge();
        my_edge->start = start;
        my_edge->end = end;
        my_edge->weight = weight;

        edges.push_back(my_edge);
    }

    float dist [vector_count][vector_count];
    float prev [vector_count][vector_count];
    
    // Set dist and prev to infinity
    for(size_t i = 0; i < vector_count; i++)
    {
        for(size_t j = 0; j < vector_count; j++)
        {
            dist[i][j] = std::numeric_limits<float>::infinity();
            prev[i][j] = std::numeric_limits<float>::infinity();
        }   
    }

    // Set the initial matrix to just include edges
    for(int i = 0; i < edges.size(); i++)
    {
        dist[edges[i]->end][edges[i]->start] = edges[i]->weight;
    }

    for(size_t k = 0; k < vector_count; k++)
    {
        for(size_t i = 0; i < vector_count; i++)
        {
            std::cout << dist[k][i] << " ";
        }
        std::cout << std::endl;
    }

    std::cout << std::string('*', 30) << std::endl;


    // For k times, optimize the routes for i and j
    for(size_t k = 0; k < vector_count; k++)
    {
        for(size_t i = 0; i < vector_count; i++)
        {
            for(size_t j = 0; j < vector_count; j++)
            {
                if(dist[i][j] > dist[i][k] + dist[k][j])
                {
                    dist[i][j] = dist[i][k] + dist[k][j];
                }
            }
        }   
    }

    // std::cout << std::setw(5);
    for(size_t k = 0; k < vector_count; k++)
    {
        for(size_t i = 0; i < vector_count; i++)
        {
            std::cout << dist[k][i] << " ";
        }
        std::cout << std::endl;
    }

    for(auto it = edges.begin(); it != edges.end(); it++)
    {
        delete *it;
    }
    
    return 0;
}