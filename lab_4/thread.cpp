#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>
#include <cstdlib>
#include <fstream>
#include <thread>
#include <mutex>
#include <stack>

float** dist;
int** pred;

pthread_barrier_t barrier;
std::mutex a;
const INT_MAX = 2147483647;

void partitionFloyd(long rank, int nodes, int vector_count)
{
    /*

    // initilization
    for i <- my_id * n / p to  (my_id) * n/p - 1
        for j <- 0 to n-1
            dist[i,j] <- Aij
            pred[i,j] <- nil
        endfor
    endfor

    // dynamic programming
    for k <- 0 to n - 1
        -- sync all threads --
        for i <- my_id * n/p to (my_id + 1) * n/p - 1
            for j <- 0 to n-1
                if(dist[i,j] > dist[i,k] + dist[k,j])
                    dist[i,j] <- dist[i,k] + dist[k,j]
                    pred[i,j] <- k
                endfor
            endfor
        endfor
    endfor

    */
   


   for(size_t k = 0; k < vector_count; k++)
   {
       pthread_barrier_wait(&barrier);
       for(size_t i = rank * vector_count / nodes; i < ((rank + 1) * vector_count / nodes) - 1; i++)
       {
           for(size_t j = 0; j < vector_count; j++)
           {
                if(dist[i][j] > dist[i][k] + dist[k][j])
                {
                    dist[i][j] = dist[i][k] + dist[k][j];
                    pred[i][j] = k;
                }
           }
       }
   }
    pthread_barrier_wait(&barrier);
//    std::cout << "got here " << std::endl;
   return;
}

int main(int argc, char* argv[])
{
    char* filename;
    int thread_count;
    if(argc == 3)
    {
        thread_count = std::atoi(argv[1]);
        filename = argv[2];
    }
    else
    {
        std::cout << "Provide correct parameters: ./thread [#-of-threads] [filename]" << std::endl << std::endl;
        return 1;
    }


    // Get data from file
    std::ifstream file(filename, std::ios::in);
    std::string str;
    file >> str;

    // std::cout << str << std::endl;
    int vector_count = std::atoi(str.c_str());

    // Initilize the global arrays
    dist = new float*[vector_count];
    pred = new int*[vector_count];
    for(size_t i = 0; i < vector_count; i++)
    {
        dist[i] = new float[vector_count];
        pred[i] = int float[vector_count];
    }

    // Set dist and prev to infinity
    for(size_t i = 0; i < vector_count; i++)
    {
        for(size_t j = 0; j < vector_count; j++)
        {
            dist[i][j] = std::numeric_limits<float>::infinity();
            pred[i][j] = INT_MAX;
        }   
    }

    std::vector<std::string> cities;
    for(size_t i = 0; i < vector_count; i++)
    {
        file >> str;
        cities.push_back(str);
    }

    while(true)
    {
        file >> str;
        int start = std::atoi(str.c_str());
        if(start == -1)
        {
            break;
        }
        file >> str;
        int end = std::atoi(str.c_str());
        file >> str;
        int weight = std::atoi(str.c_str());

        // Make sure to make it bi-directional
        dist[start][end] = weight;
        dist[end][start] = weight;
    }

    std::vector<std::thread> thread_handles;

    // Print Array
    for(size_t k = 0; k < vector_count; k++)
    {
        for(size_t i = 0; i < vector_count; i++)
        {
            std::cout << std::setw(5);
            std::cout << std::left;
            std::cout << dist[k][i] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << std::endl;

    // Call threads
    pthread_barrier_init(&barrier, NULL, thread_count);
    for(long t = 0; t < thread_count; t++)
    {
        thread_handles.push_back(std::thread(partitionFloyd, t, thread_count, vector_count));
    }

    // Print Array
    for(size_t k = 0; k < vector_count; k++)
    {
        for(size_t i = 0; i < vector_count; i++)
        {
            std::cout << std::setw(5);
            std::cout << std::left;
            std::cout << dist[k][i] << " ";
        }
        std::cout << std::endl;
    }

    // Destroy threads

    for(long t = 0; t < thread_count; t++)
    {
        thread_handles[t].join();
    }
    pthread_barrier_destroy(&barrier);

    // Menu loop

    while(true)
    {

        for(size_t i = 0; i < cities.size(); i++)
        {
            std::cout << i + 1 << ".: " << cities[i] << std::endl;
        }
        std::cout << "q. quit" 

        std::cout << "Starting location:";
        std::stirng startStr;
        std::cin >> startStr;
        if(startStr == "q")
        {
            break;
        }

        std::cout << "Ending location"
        std::string endStr;
        std::cin >> endStr;
        if(startStr == "q")
        {
            break;
        }

        int start = std::atoi(startStr.c_str());
        int end = std::atoi(endStr.c_str());

        std::stack<int> route;

        int i = end;
        do
        {
            i = pred[start][i];
            route.push_back(i);
        } while (i != INT_MAX);
        
    }

    return 0;
}