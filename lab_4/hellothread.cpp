/*
    From parallel programming slides (ipp13), Hello World example
*/

#include <iostream>
#include <vector>
#include <thread>

int thread_count;
void hello(long my_rank)
{
    std::cout << "Hello from thread " << my_rank << " of " << thread_count << std::endl;
    return;
}

int main(int argc, char* argv[])
{
    // Use a long in case of 64-bit systems
    long t = 0;
    std::vector<std::thread> thread_handles;
    // Get number of threads from command line
    thread_count = std::strtol(argv[1], NULL, 10);

    
    for(; t < thread_count; t++)
    {
        thread_handles.push_back(std::thread(hello, t));
    }

    std::cout << "Hello from the main thread \n";

    for(t = 0; t < thread_count; t++)
    {
        thread_handles[t].join();
    }

    return 0;
}